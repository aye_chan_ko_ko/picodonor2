import React from "react";
import './homePageStyle.css'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory,
  useLocation,
  useParams
} from "react-router-dom";
import HomePage from './HomePage';
import Share from './Share'
import InnerAHluShin from "./InnerAHluShin";
import AHluKhan from './AHluKhan';
import InnerAHluKhan from './InnerAHluKhan';
import RegistrationForm from "./RegistrationForm";
import SaYinMyar from './SaYinMyar';
import SeSinThu from "./SeSinThu";
import Item from "./Item";
import PPE from "./PPE";
import CCTV from "./CCTV";
export default function LinkToTest() {
    return (
      <div>
  <Router>
        <ModalSwitch />
      </Router>
      
      </div>
      
    );
  }
  
  function ModalSwitch() {
    let location = useLocation();
    let history = useHistory();
    let background = location.state && location.state.background;
  
    return (
      <div>
        <Switch location={background || location}>
          <Route exact path="/" children={<HomePage />} />
          <Route exact path="/ahlukhan" children={<AHluKhan/>}/>
          <Route exact path="/ahlushin" children={<HomePage/>}/>
          <Route exact path="/sayinmyar" children={<SaYinMyar/>}/>
          <Route path="/sharePage" children={<Share />} />
          <Route path="/sesinthu" children={<SeSinThu />} />
          <Route path="/item" children={<Item />} />
          <Route path="/ppe" children={<PPE />} />
          <Route path="/cctv" children={<CCTV />} />
          <Route path="/registrationForm" children={<RegistrationForm />} />
          <Route path="/ahlushin/:id" children={<InnerAHluShin />} />
          <Route path="/ahlukhan/:id" children={<InnerAHluKhan />} />
          {/* <Route path="/img/:id" children={<ImageView />} /> */}
        </Switch>
  
        {/* Show the modal when a background page is set */}
        {/* 8 */}
      </div>
    );
  }
 


  
  
  
