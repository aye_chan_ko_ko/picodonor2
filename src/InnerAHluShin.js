import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory,
    useLocation,
    useParams
  } from "react-router-dom";
const DonerName=[
    {id:1,name:"Aye Chan",value:"PPE + Mask",price:180000,imgName:require('../src/person.jpg')},{id:2,name:"Lin Lin",value:"PPE + Mask",price:180000,},
    {id:3,name:"Mya Mya",value:"PPE + Mask",price:180000,},{id:4,name:"Tun Tun",value:"PPE + Mask",price:180000,},
    {id:5,name:"Su Nandar",value:"PPE + Mask",price:180000,},{id:6,name:"Maw Maw",value:"PPE + Mask",price:180000,},
    {id:7,name:"Khaing Ngwe",value:"PPE + Mask",price:180000,},{id:8,name:"Maung Maung",value:"PPE + Mask",price:180000,},
    {id:9,name:"Aye Aung",value:"PPE + Mask",price:180000,},{id:10,name:"Aye Aye",value:"PPE + Mask",price:180000,},
    {id:11,name:"Aung Phyo Wai",value:"PPE + Mask",price:180000,},{id:12,name:"Aye Chan Thu",value:"PPE + Mask",price:180000,},
    {id:13,name:"Su Su",value:"PPE + Mask",price:180000,},{id:14,name:"Nu Nu",value:"PPE + Mask",price:180000,}
]
export default function InnerAHluShin() {
    let history = useHistory();
    console.log(history)
    let back = e => {
       e.stopPropagation();
       history.goBack();
       };
      let { id } = useParams();
      let image = DonerName[parseInt(id-1, 10)];
    // console.log(id)
      if (!image) return <div>Image not found</div>;
    
      return (
        <div>
          <nav className="navbar navbar-expand-sm w3-green fixed-top" style={{width: 100+'%',height: 50+'px'}}>
              <div>
                  <a href="#"><i class='fas fa-arrow-left' onClick={back} style={{fontSize:20+'px',color:'white'}}>{image.name} </i></a>
              </div>            
          </nav>
          <div style={{position:'absolute',left:20,top:70}}>
              <h3>{image.name}</h3>
              <p>PPE+Mask</p><br/>
              <p>Qty</p>
              <h5>10</h5><br/>
              <p>Amount</p>
              <p>180000</p><br/>
              <p>Hospitals to Donate</p>
              <p>Random</p><br/>
              <p>Payment</p>
              <p>Yes</p>
  
          </div>
          <div className="footer">
              <table id="changeTable2">
                  <tr style={{color:"black"}}> 
                      <td><i class="fas fa-donate" style={{color:'black'}}></i><br/>အလှူရှင်</td>
                      <td><i class="fa fa-medkit" style={{color:'black'}}></i><br/>အလှူခံ</td>
                      <td><i class="fas fa-newspaper" style={{color:'black'}}></i><br/>စာရင်းများ</td>
                      <td><i class="fas fa-comment" style={{color:'black'}}></i><br/>Chat</td>
                  </tr>
              </table>
            </div>
        </div>
      );
    }
  