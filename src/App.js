import React from 'react';
import logo from './logo.svg';
import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;

// import{
//   BrowserRouter as Router,
//   Switch,
//   Router,
//   Link,
// } from "react-router-dom";

// const users=[
//   {id:1, name:'Alice',gender:'f'},
//   {id:2, name:'bob', gender:'m'},
//   {id:3, name:'Tom', gender:'m'},
//   {id:4, name:'Mary', gender:'f'},
// ]
// const Male=props=>{
//   return(
//     <ul>
//       {users.filter(u=>u.gender==='m').map(u=><li key={u.id}>{u.name}</li>)}
//     </ul>
//   );
// }
// const Female=props=>{
//   return(
//     <ul>
//       {users.filter(u=>u.gender==='f').map(u=><li key={u.id}>{u.name}</li>)}
//     </ul>
//   );
// }

// const App=props=>{
//   return(
//     <Router>
//       <div>
//         <ul>
//           <li><Link to="/male">Male</Link></li>
//           <li><Link to="/femlae">Female</Link></li>
//         </ul>
//       </div>
//       <div style={{background:'cyan', padding:20}}>
//         <Switch>
//           <Route path="/male"><Male/></Route>
//           <Route path="/female"><Female/></Route>
//         </Switch>
//       </div>
//     </Router>
//   );
// }


import React from 'react';
import logo from './logo.svg';
import './App.css';
import HelloWorldTest from "./HelloWorldTest"
import Hey from "./Hey"

function App() {
  return <React.Fragment>
    <HelloWorldTest />
    <Hey />
    </React.Fragment>
}
export default App;